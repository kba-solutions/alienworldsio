﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlienWorldsAuto2
{
    public partial class MainForm : Form
    {
        AlienWorldAutomator automator = new AlienWorldAutomator();
        List<ProfileData> listProfiles = new List<ProfileData>();

        List<ProfileData> queues = new List<ProfileData>();
        Mutex mutex = new Mutex();

        ProfileData currentProfile = new ProfileData();

        int prevIndex = -1;

        List<Thread> threads = null;

        public MainForm()
        {
            InitializeComponent();
            tb_Thread.Value = 5;
            tb_Delay.Value = 10;
        }
        private ProfileData GetSelectedProfile()
        {
            if (dataGridView1.SelectedRows.Count > 0 && listProfiles.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                prevIndex = index;
                return listProfiles[index];
            }
            return null;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null && queues.Find(i => i.Account == profile.Account) == null)
            {
                queues.Add(profile);
                ReloadProfiles();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        //nguyenvanhuonghsg@gmail.com
        //Dunghackemnha1
        //odorahmong@outlook.com
        //omari.hills5@gmail.com
        //Q!ad#7639.
        //vi4ds.wam 

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            automator.CreateProfile(GetProfileName());
            ReloadProfiles();
        }

        private void ReloadProfiles()
        {
            var sourceList = new BindingList<ProfileData>(listProfiles.Select(p =>
            {
                p.Queue = queues.FindIndex(i => p.Account == i.Account);

                return p;
            }).ToList());
            try
            {
                dataGridView1.DataSource = sourceList;
                dataGridView1.Rows[prevIndex].Selected = true;
                dataGridView1.CurrentCell = dataGridView1.Rows[prevIndex].Cells[0];
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }


        }
        private void LoadProfiles()
        {
            listProfiles = automator.LoadProfiles();

            var sourceList = new BindingList<ProfileData>(listProfiles.Select(p =>
            {
                p.Queue = queues.FindIndex(i => p.Account == i.Account);
                return p;
            }).ToList());

            dataGridView1.DataSource = sourceList;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            LoadProfiles();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (GetSelectedProfile() != null)
            {
                automator.SetupProfile(GetSelectedProfile().ProfileDir, tb_Email.Text, tb_Password.Text);
            }
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null)
            {
                try
                {
                    queues.RemoveAt(queues.FindIndex(i => i.Account == profile.Account));
                    ReloadProfiles();
                }
                catch
                {

                }

            }
        }

        Thread getRunThread(int number)
        {
            MethodInvoker methodInvoker = delegate
            {
                ReloadProfiles();
            };
            return new Thread(() =>
            {
                while (true)
                {

                    for (int i = 0; i < queues.Count; i++)
                    {
                        if (i % Math.Floor(tb_Thread.Value) == number)
                        {
                            var profile = queues[i];
                            profile.Status = "Running";
                            if (DateTimeOffset.Now.ToUnixTimeSeconds() - profile.Last < profile.Delay * 60)
                                continue;
                            profile.Last = DateTimeOffset.Now.ToUnixTimeSeconds();
                            this.Invoke(methodInvoker);
                            automator.ClaimCoin(profile.Account, profile.ProfileDir, cb_vpn.Checked);
                            profile.Status = "Waiting";
                            this.Invoke(methodInvoker);
                        }
                        Thread.Sleep(10000);
                    }

                }
            });
        }
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (threads == null)
            {
                automator.killDriver();
                threads = new List<Thread>();
                if (queues.Count == 0) return;
                for (int i = 0; i < Math.Floor(tb_Thread.Value); i++)
                {
                    threads.Add(getRunThread(i));
                }
                threads.ForEach(t => t.Start());
                button3.Text = "Stop";
            }
            else
            {
                button3.Text = "Start";
                threads?.ForEach(t =>
                {
                    try
                    {
                        t.Abort();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                    }

                });
                Thread.Sleep(1000);
                automator.killDriver();
                threads = null;

                LoadAfterUpdate();
                listProfiles.ForEach(p => p.Status = "Waiting");
                ReloadProfiles();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            threads?.ForEach(t =>
            {
                try
                {
                    t.Abort();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }

            });
            Thread.Sleep(1000);
            automator.killDriver();
            threads = null;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null)
            {
                Process.Start("notepad.exe", $"Chrome\\Script\\{profile.Account}.js");
            }
           
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null)
                Directory.Delete("Chrome\\Profile\\" + profile.ProfileDir, true);
            LoadAfterUpdate();
        }

        private void LoadAfterUpdate()
        {
            LoadProfiles();
            ReloadProfiles();
            queues.Clear();
        }
        private string GetProfileName()
        {
            return tb_Account.Text + "^" + tb_Delay.Text;
        }
        private void button9_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null)
                Directory.Move("Chrome\\Profile\\" + profile.ProfileDir, "Chrome\\Profile\\" + GetProfileName());
            LoadAfterUpdate();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            var profile = GetSelectedProfile();
            if (profile != null)
            {
                automator.OpenProfile(profile.ProfileDir);
            }
        }
    }
}
