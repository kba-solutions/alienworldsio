﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AlienWorldsAuto2
{
    class AlienWorldAutomator
    {
        Random rand = new Random(Guid.NewGuid().GetHashCode());
        public void StartChromeWithProfile(string profile)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            string cmdRun = $"Chrome\\chrome.exe --user-data-dir=\"{profile}\" --remote-debugging-port=9083 --no-first-run --disable-popup-blocking";
            Console.WriteLine(cmdRun);
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + cmdRun;
            process.StartInfo = startInfo;
            process.Start();
        }

        public bool CreateProfile(string name)
        {
            killDriver();

            string profilePath = "Chrome\\Profile\\" + name.Trim();
            if (name.Length == 0 || name.Trim().Contains(" ") || Directory.Exists(profilePath)) return false;

            try
            {
                File.Copy(@"Chrome\Extension\script.js", $"Chrome\\Script\\{GetProfileData(name).Account}.js");
                StartChromeWithProfile(profilePath);
                ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                ChromeOptions opt = new ChromeOptions();
                opt.DebuggerAddress = "127.0.0.1:9083";
                ChromeDriver driver = new ChromeDriver(driverService, opt);
                SetImplicitWait(driver, 60000);
                driver.Navigate().GoToUrl("https://chrome.google.com/webstore/detail/surfshark-vpn-extension-f/ailoabdmgclmfmhdagmlohpjlbpffblp");
            }
            catch
            {
                killDriver();
            }
            return true;
        }
        public bool OpenProfile(string name)
        {
            killDriver();
            string profilePath = "Chrome\\Profile\\" + name.Trim();
            if (name.Length == 0 || name.Trim().Contains(" ") || !Directory.Exists(profilePath)) return false;
            try
            {
                StartChromeWithProfile(profilePath);
                ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                ChromeOptions opt = new ChromeOptions();
                opt.DebuggerAddress = "127.0.0.1:9083";
                ChromeDriver driver = new ChromeDriver(driverService, opt);
                SetImplicitWait(driver, 60000);
                driver.Navigate().GoToUrl("https://play.alienworlds.io");
            }
            catch
            {

            }
            return true;
        }
        public bool SetupProfile(string name, string email, string password)
        {
            killDriver();
            string profilePath = "Chrome\\Profile\\" + name.Trim();
            if (name.Length == 0 || name.Trim().Contains(" ") || !Directory.Exists(profilePath)) return false;
            try
            {
                StartChromeWithProfile(profilePath);
                ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                ChromeOptions opt = new ChromeOptions();
                opt.DebuggerAddress = "127.0.0.1:9083";
                ChromeDriver driver = new ChromeDriver(driverService, opt);
                SetImplicitWait(driver, 60000);
                //driver.Navigate().GoToUrl("https://stable.getautoclicker.com/");
                //driver.FindElementById("import-configurations").SendKeys(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + "Chrome\\Extension\\config.json");
                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("chrome-extension://ailoabdmgclmfmhdagmlohpjlbpffblp/index.html");
                driver.FindElementByName("email").SendKeys(email);
                driver.FindElementByName("password").SendKeys(password);
                driver.FindElementByCssSelector("button[type='submit']").Click();
                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://mail.google.com");
            }
            catch
            {

            }
            return true;
        }
        private static Semaphore s = new Semaphore(1, 2);


        public bool IsWalletCPUGood(string wallet)
        {
            s.WaitOne();
            Thread.Sleep(500);
            try
            {
                int result = GetCPU(wallet);
                if (result >= 0 && result < 95)
                {

                    s.Release();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            s.Release();
            return false;
        }
        public bool ClaimCoin(string wallet, string profileDir, bool useVpn)
        {
            ChromeDriver driver = null;
            string profilePath = "Chrome\\Profile\\" + profileDir.Trim();
            if (wallet.Length == 0 || wallet.Trim().Contains(" ") || !Directory.Exists(profilePath)) return false;
            try
            {
                var isWalletGood = IsWalletCPUGood(wallet);
                Console.WriteLine("isWaletGood " + wallet + ", " + isWalletGood);
                if (!isWalletGood)
                    return false;
                //StartChromeWithProfile(profilePath);
                ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                ChromeOptions opt = new ChromeOptions();
                opt.BinaryLocation = "Chrome\\chrome.exe";
                opt.AddArgument($"user-data-dir={profilePath}");
                if (!useVpn)
                    opt.AddArguments("--disable-extensions");
                opt.AddExcludedArgument("enable-automation");
                //opt.DebuggerAddress = "127.0.0.1:9083";
                driver = new ChromeDriver(driverService, opt);
                SetImplicitWait(driver, 60000);
                if (useVpn)
                {
                    driver.Navigate().GoToUrl("chrome-extension://ailoabdmgclmfmhdagmlohpjlbpffblp/index.html");
                    var connectElems = driver.FindElementsByXPath("//span[text()='Locations']/../div/div/div");
                    Thread.Sleep(2000);
                    connectElems[rand.Next(0, connectElems.Count - 3)].Click();
                    Thread.Sleep(5000);
                }
                driver.Navigate().GoToUrl("https://play.alienworlds.io/");
                driver.FindElementById("#canvas");
                Thread.Sleep(5000);


                var script = File.ReadAllText($"Chrome\\Script\\{wallet}.js");
                driver.ExecuteScript(script);
                int checkCount = 0;
                while (true && checkCount++ < 150)
                {
                    Thread.Sleep(2000);
                    driver.Manage().Logs.GetLog(LogType.Browser).ToList();

                    if (!IsWalletCPUGood(wallet) || driver.Manage().Logs.GetLog(LogType.Browser).ToList().FindIndex(l => l.Message.ToLower().Contains("claim thành công")) > 0)
                    {
                        driver.Quit();
                        return true;
                    }
                    try
                    {
                        driver.SwitchTo().Window(driver.WindowHandles[driver.WindowHandles.Count - 1]);
                        SetImplicitWait(driver, 2000);
                        driver.FindElementByXPath("//div[text()='Approve']").Click();
                        SetImplicitWait(driver, 60000);
                        Thread.Sleep(5000);
                        driver.Quit();
                        return true;
                    }
                    catch
                    {

                    }
                    SetImplicitWait(driver, 60000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            driver?.Quit();
            return true;
        }
        public ProfileData GetProfileData(string name)
        {
            try
            {
                var values = name.Split('^');
                var profile = new ProfileData();
                profile.ProfileDir = name;
                profile.Account = values[0];
                profile.Delay = Int32.Parse(values[1]);
                profile.Status = "Loaded";
                profile.Queue = -1;
                return profile;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public List<ProfileData> LoadProfiles()
        {
            DirectoryInfo dInfo = new DirectoryInfo("Chrome\\Profile");
            DirectoryInfo[] subdirs = dInfo.GetDirectories();
            return subdirs.Select((d, i) =>
            {
                return GetProfileData(d.Name);
            }).Where(p => p != null).ToList();
        }
        public int GetCPU(string wallet, int retry = 0)
        {
            int result = -1;
            try
            {
                if (string.IsNullOrEmpty(wallet))
                {
                    result = -1;
                }
                else
                {
                    Thread.Sleep(5000);
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wax.greymass.com/v1/chain/get_account");
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = $"{{\"account_name\":\"{wallet.Trim()}\"}}";

                        streamWriter.Write(json);
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var text = streamReader.ReadToEnd();

                        if (text.Length > 0)
                        {
                            ResultAccountWAX resultAccountWAX = JsonConvert.DeserializeObject<ResultAccountWAX>(text);
                            if (resultAccountWAX.cpu_limit != null)
                            {
                                result = Convert.ToInt32((double)resultAccountWAX.cpu_limit.used / (double)resultAccountWAX.cpu_limit.max * 100.0);
                            }
                        }
                        else
                        {
                            Thread.Sleep(15000);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Thread.Sleep(30000);
            }
            return result;
        }
        public void killDriver()
        {
            try
            {
                foreach (var process in Process.GetProcessesByName("chromedriver"))
                {
                    process.Kill();
                }
                foreach (var process in Process.GetProcessesByName("chrome"))
                {
                    process.Kill();
                }
            }
            catch (Exception e)
            {

            }
        }
        public void SetImplicitWait(ChromeDriver driver, int milisec)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(milisec);
        }
        public byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
