﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlienWorldsAuto2
{
    class ProfileData
    {
        //public int No { get; set; }
        public string Account { get; set; }
        public string Status { get; set; }
        public int Queue { get; set; }
        public int Delay { get; set; }
        public string ProfileDir { get; set; }
        public long Last { get; set; }

    }
}
