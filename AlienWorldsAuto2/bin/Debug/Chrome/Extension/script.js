//auto
var may = '5';
let auto = async () => {
  let account = await wax.login();
  let llog = (msg, color = "#222", bgcolor = "#bada55") =>
    console.log(`%c [HieuLe]: ${msg}`, `background: ${bgcolor}; color: ${color}`);
  const timeout = (prom, time) =>
    Promise.race([prom, new Promise((_r, rej) => setTimeout(rej, time))]);
  const sleep = (miliseconds) => new Promise((resolve) => setTimeout(resolve, miliseconds));
  const lbackground_mine = async (account) => {
    return new Promise(async (resolve, reject) => {
      const bagDifficulty = await getBagDifficulty(account);
      const landDifficulty = await getLandDifficulty(account);
      const difficulty = bagDifficulty + landDifficulty;

      if (isNaN(parseInt(difficulty))) {
        reject(Error(`Lỗi không lấy được giá trị difficulty: ${difficulty}`));
      }
      console.log("difficulty", difficulty);

      console.log("start doWork = " + Date.now());
      const last_mine_tx = await lastMineTx(mining_account, account, wax.api.rpc);

      doWorkWorker({ mining_account, account, difficulty, last_mine_tx }).then((mine_work) => {
        console.log("end doWork = " + Date.now());
        resolve(mine_work);
      });
    });
  };

  const lget_bounty_from_tx = async (transaction_id, miner, hyperion_endpoints) => {
    return new Promise(async (resolve, reject) => {
      for (let i = 0; i < 30; i++) {
        for (let h = 0; h < hyperion_endpoints.length; h++) {
          const hyp = hyperion_endpoints[h];
          if (hyp != "https://wax.eosusa.news") {
            try {
              const url = `${hyp}/v2/history/get_transaction?id=${transaction_id}`;
              const t_res = await fetch(url);
              const t_json = await t_res.json();

              if (t_json.executed) {
                let amount = 0;
                const amounts = t_json.actions
                  .filter((a) => a.act.name === "transfer")
                  .map((a) => a.act)
                  .filter((a) => a.data.to === miner)
                  .map((a) => a.data.quantity);
                amounts.forEach((a) => (amount += parseFloat(a)));
                if (amount > 0) {
                  resolve(`${amount.toFixed(4)} TLM`);
                  return;
                }
              }
            } catch (e) {
              reject(e.message);
            }
          }

          await sleep(1000);
        }

        await sleep(2000);
      }

      resolve("UNKNOWN");
    });
  };

  const lclaim = (
    mining_account,
    account,
    account_permission,
    mine_data,
    hyperion_endpoints,
    eos_api
  ) => {
    return new Promise(async (resolve, reject) => {
      try {
        const actions = [
          {
            account: mining_account,
            name: "mine",
            authorization: [
              {
                actor: account,
                permission: account_permission,
              },
            ],
            data: mine_data,
          },
        ];
        const res = await eos_api.transact(
          {
            actions,
          },
          {
            blocksBehind: 3,
            expireSeconds: 5 * 360,
          }
        );

        console.log(res.transaction_id);
        try {
          llog("Đã claim thành công, đang kiểm tra số tiền đào được...");
          await sleep(4000);
          const amount = await lget_bounty_from_tx(res.transaction_id, account, hyperion_endpoints);
          console.log(`${account} mined ${amount}`);
          resolve(amount);
        } catch (e) {
          llog("What the fuck, kiểm tra thất bại. Nhưng vẫn có tiền");
          resolve("UNKNOWN");
        }
      } catch (e) {
        console.log(`Failed to push mine results ${e.message}`);
        reject(e);
      }
    });
  };

  let lmine = async () => {
    try {
      llog("Giữ bình tình đi.... đang tiến hành đào");
      let mine_work = await lbackground_mine(account);

      if (!mine_work) {

        var req = new XMLHttpRequest();
          req.open("GET", "https://api.telegram.org/bot1809353390:AAGEUvZhnUkP2zD1ntZX3ZcepXsjfZW0lzo/sendMessage?chat_id=-440663340&text=[MAY "+may+"] Đào thất bại", false);
          req.send(null);

        throw Error(`Đào thất bại: ~ mine_work: ${mine_work}`);
      }

      llog("Đào xong rồi, Claim nhận tiền.....");
      let mine_data = { miner: mine_work.account, nonce: mine_work.rand_str };
      for (let i = 0; i < 3; i++) {
        let promiseClaim = lclaim(
          mining_account,
          account,
          "active",
          mine_data,
          ["https://api.waxsweden.org", "https://wax.eosrio.io"],
          wax.api
        );

        try {
          let tlm = await timeout(promiseClaim, 60 * 5 * 1000);
          if (tlm != "UNKNOWN") {
            unityInstance.SendMessage("Controller", "Server_Response_Claim", tlm);
          }
          var req = new XMLHttpRequest();
          req.open("GET", "https://api.telegram.org/bot1809353390:AAGEUvZhnUkP2zD1ntZX3ZcepXsjfZW0lzo/sendMessage?chat_id=-440663340&text=[MAY "+may+"] nhận được "+tlm, false);
          req.send(null);

          llog(`Claim thành công, nhận được: ${tlm}`, "red", "yellow");
          waitNext();
          return;
        } catch (e) {
          llog(`[ERROR][Lần ${i + 1}] Claim thất bại: ${(e || {}).message} `, "yellow", "red");
        }
      }
    } catch (err) {
      llog(`[ERROR] CLAIM: ${err.message}`, "yellow", "red");
    }
    waitNext();
  };

  let waitNext = async () => {
    let timeout = await getMineDelay(account);
    unityInstance.SendMessage("Controller", "Server_Response_GetMineDelay", timeout.toString());
    llog(`Đợi thêm ${timeout / 1000}s nữa để đào tiếp`);
    setTimeout(lmine, timeout + 2000);
  };

  waitNext();
};
auto();
//Bản cập nhật: lần 4
