// @see: https://bugzilla.mozilla.org/show_bug.cgi?id=1355198
const DIRECT = 'DIRECT';

const currentProxy = {
  config: { mode: 'system' },
  configString: DIRECT,
};

function setProxyConfig(config) {
  if (typeof config === 'undefined') {
    return;
  }

  currentProxy.config = config;

  if (config.mode === 'system' || config.mode === 'direct') {
    currentProxy.configString = DIRECT;

    browser.runtime.sendMessage({ type: 'surfshark.proxy.disabled' });
  } else {
    const proxy = config.rules.singleProxy;
    const scheme = proxy.scheme.toUpperCase();
    const { host, port } = proxy;

    currentProxy.configString = `${scheme} ${host}:${port}`;

    browser.runtime.sendMessage({ type: 'surfshark.proxy.enabled' });
  }
}

browser.runtime.onMessage.addListener(
  ({ type, config }, sender, sendResponse) => {
    if (type === 'proxy') {
      setProxyConfig(config);
    } else if (type === 'status') {
      sendResponse(currentProxy.config);
    }
  },
);

// eslint-disable-next-line no-unused-vars
function FindProxyForURL(url, host) {
  // @see: https://bugzilla.mozilla.org/show_bug.cgi?id=1353510
  const isIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(host);
  const isPrivateIP =
    isIP &&
    (isInNet(host, '10.0.0.0', '255.0.0.0') ||
      isInNet(host, '172.16.0.0', '255.240.0.0') ||
      isInNet(host, '192.168.0.0', '255.255.0.0') ||
      isInNet(host, '127.0.0.0', '255.255.255.0'));

  if (isPrivateIP) {
    return DIRECT;
  }

  if (host === 'localhost' || host === 'localhost.localdomain') {
    return DIRECT;
  }
  return currentProxy.configString;
}
